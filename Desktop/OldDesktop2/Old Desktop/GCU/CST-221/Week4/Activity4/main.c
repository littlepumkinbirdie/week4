//
//  main.c
//  Activity4
//
//  Created by Josh Beck on 10/21/20.
//
//  This program accepts input from the user and manually transforms the decimal
//  value into a binary and hexadecimal number.  It does this through transformation
//  between the bases and will output the transformations.  Additionally, we
//  experiment with bit masks and operations in order to manipulate the answer
//  entered in at the beginning of the program.

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stdint.h>

uint32_t convertBinary(uint32_t num, char* binary[], uint32_t i){
    
    //Convert the number to a character by appending a '0'
    binary[i] = ((num % 2) + '0');
    
    //Recursively call the binary function until the number is 0 or smaller (indicating that we have tried to halve a 1 as we are dealing with an integer type)
    if (num/2 != 0){
        return convertBinary(num/2, binary, i + 1);
    } else {
        //We have reached the end of the recursive function so return the last passed value of i
        return i;
    }
    
    
}
/**
 Helper function to get the binary number from a decimal.  It trims any extra characters from the string and returns a shortened binary string
 */
char* getBinary(uint32_t num){
    char* binary[32];
    
    uint32_t j = convertBinary(num, binary, 0);
    char* n = malloc(j + 1);

    for (int i = j; i >= 0; i--){
        n[j - i] = binary[i];
    }
    return n;
}
/**
 This method takes a binary nibble as an input and returns the appropriate number
 */
uint32_t convertNibbleToNumber(uint32_t num){
    //Convert binary to string
    
    //Get size of the number
    uint32_t length = snprintf( NULL, 0, "%d", num ) + 1;
    char* str = malloc(length);
    //Create string from the number
    snprintf( str, length, "%d", num);
    
    uint32_t finalNum = 0;
    for (int i = 0; i < length - 1; i++){

        if (str[i] == '1'){
            //Raise to appropriate power of 2 offset by the length as we are reading right to left to appropriate the power but the for loop increments left to right
            finalNum += pow(2, length - 2 - i);
        }
    }
  
    return finalNum;
}
/**
 This method converts a binary number (in string format) to a HEX string and returns the HEX string
 */
char* binaryToHex(char* str, int length){
    //Convert binary to string
   
    /*Add additional "cushion" zeros so there is no index out of range error when
         moving from right to left while iterating through the binary number in the
         next step*/

    /*This number determines how many nibbles there are in the binary number and
        rounds this number up*/
    int max = ceil((float)(length)/4.0f);
    
    //Create a string to hold the completed HEX string to be returned later
    char* completeHEXStr = malloc(max + 1);
  
    //Iterate through each nibble and evaluate the appropriate HEX character
    for (int i = 0; i < max; i++){
        uint32_t nibble = 0;

        //Iterate and fetch each part of the nibble
        for (int j = 0; j < 4; j++){
            //Get the offset starting from the smallest unit but also recognizing that there is not inherently enough digits to equal a full nibble (ex. 10 binary digits will round to 12 digits [4 nibbles]).  Therefore, we must offset everything to start under 0 by offset...actual length.  Additionally, we must inverse this value so that the values are read from left to right while recognizing array bounds
            int k = (length - (max*4 - 1)) - j + ((i + 1)*4 - 1);
          
            if (k >= 0){
                if (str[k] == '1'){
                    nibble += pow(10, j);
                }
            }
               
        }
      
        //Hold the character of the corrosponding HEX value to be appended to the completed HEX string later
        char HEX = '0';
      
        //Use this function to get the number of the nibble
        uint32_t actualNum = convertNibbleToNumber(nibble);
       
        //Determine the right character
        if (actualNum < 10){
            HEX = actualNum + '0';
        } else {
            switch (actualNum){
                case 10:
                    HEX = 'A';
                    break;
                case 11:
                    HEX = 'B';
                    break;
                case 12:
                    HEX = 'C';
                    break;
                case 13:
                    HEX = 'D';
                    break;
                case 14:
                    HEX = 'E';
                    break;
                case 15:
                    HEX = 'F';
                    break;
                default:
                    printf("Invalid input!\n");
                    break;
            }
        }
        //Append to the HEX string
        completeHEXStr[i] = HEX;
       
    }
    //Return the completed HEX string
    return completeHEXStr;
}
/**Helper function to convert a decimal number into a HEX.  This function is brief but is short hand for converting
 the decimal to the binary and then to the HEX*/
char* getHex(int num){
    char* binary[32];
    
    uint32_t j = convertBinary(num, binary, 0);
    char* str = malloc(j + 1);

    for (int i = j; i >= 0; i--){
        str[j - i] = binary[i];
    }
   
    return binaryToHex(str, j);
}
int handleProgram(){
    printf("Please enter a decimal between 0 and 4095...");

    uint32_t answer;

    if (scanf("%d", &answer) != 0){
        //The number was successfully saved
        
        if (answer >= 0 && answer <= 4095){
            //Continue as the number is safe and within range
            printf("The entered number (%d) to binary is 0b%s\n", answer, getBinary(answer));
            printf("The entered number (%d) to HEX 0x%s\n", answer, getHex(answer));
           
            /*Transform the input by shifting the number 16 bits to the left, then mask out (AND) the bottom 16 bits, and finally add (OR) the hex number 0x07FF to produce the final resultant number. Display the final result in binary, hexadecimal, and decimal to the console.*/
            printf("Shifting answer 16 bits to the left...\n");
            //Shift the number 16 to the left (add 16 zeros on the right of the number)
            answer <<= 16;
            printf("Current number is %d\n", answer);
            
            printf("Masking out (removing) the bottom 16 digits...\n");
            //Removing the bottom 16 bits (HEX representation of 16 ones followed by 16 digits)
            answer &= 0xFFFF;
            printf("Current numer is %d\n", answer);
            
            printf("Adding the hex number 0x07FF...\n");
            //Add the hex number
            answer |= 0x07FF;
            printf("Final answer after all manipulations is decimal: %d, binary: 0b%s, HEX: 0x%s\n", answer, getBinary(answer), getHex(answer));
            
        } else {
            //The number was outside the range so re-run the program
            printf("The number entered was not between 0 and 4095... try again...");
            handleProgram();
        }
        
    } else {
        //The number entered was not a number
        printf("The number entered was not a number... try again...");
        handleProgram();
    }
    return 0;
}

int main(){
     handleProgram();
}

